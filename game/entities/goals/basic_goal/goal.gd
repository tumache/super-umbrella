extends Area2D

var stage_transitor
var saver = preload("res://managers/saver/saver.gd").new()

signal stage_finished

func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	
	assert(finder.has_transitor())
	stage_transitor = finder.get_transitor()

func _on_goal_body_entered(body):
	if body.is_in_group("Player"):
		hide()
		emit_signal("stage_finished")
		stage_transitor.transition(stage_transitor.WIN)