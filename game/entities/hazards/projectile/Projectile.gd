extends KinematicBody2D

signal death

var hit = false

export(int, "left", "down", "right", "up") var facing = 0
export(int, 32, 1024) var pixels_per_second = 32

var alive = true
var direction

func _ready():
	var angle = - facing * PI / 4
	self.direction = Vector2(cos(PI + angle), sin(PI + angle))
	set_rotation(angle)

func _physics_process(delta):
	if hit:
		_hit(null)

	if alive:
		var move = move_and_collide(direction * pixels_per_second * delta)
		if move != null:
			hit = true

func _hit(body):
	if alive:
		emit_signal("death")
		alive = false
		$DamageArea.queue_free()
		$ProjectileAnimation.play("dying")
		yield($ProjectileAnimation, "animation_finished")
		queue_free()
