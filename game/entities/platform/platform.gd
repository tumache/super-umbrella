tool
extends KinematicBody2D

export(Vector2) var size = Vector2(64, 16) setget _set_size

func _ready():
	set_nine_patch()
	var new_shape = RectangleShape2D.new()
	new_shape.extents = size / 2
	$CollisionShape2D.shape = new_shape
	$CollisionShape2D.one_way_collision = true


func _set_size(new_size):
	size.x = max(16, new_size.x)
	size.y = max(16, new_size.y)
	
	if !has_node("CollisionShape2D") or !$CollisionShape2D.shape:
		return
	
	$CollisionShape2D.shape.extents = size / 2
	set_nine_patch()


func set_nine_patch():
	$NinePatchRect.rect_size = size
	$NinePatchRect.rect_position = - $NinePatchRect.rect_size / 2
