extends Polygon2D

signal finished
export(String, MULTILINE) var original_text = "Hello \n\n World"

var text
var pause_manager

var pages 		= 0
var cur_page	= 0

var page_chars 	= 0
var cur_char	= 0

func new(dialogue):
	text = dialogue.split("\n\n")
	
func _ready():
	var finder = preload("res://managers/finder/Finder.gd").new(get_tree())
	
	assert(finder.has_pauser())
	pause_manager = finder.get_pauser()
	
	pause_manager.speech_pause()
	
	if text == null:
		text = original_text.split("\n\n")
	
	pages = text.size()
	
	text[0] = text[0].replace("\n"," ")
	page_chars = text[0].length()
	$Text.set_bbcode(text[0])
	$Text.set_visible_characters(0)
	$Timer.start()

func _process(delta):
	if Input.is_action_just_pressed("action_jump"):
		if cur_char == page_chars:
			cur_page += 1
			
			if cur_page >= pages:
				pause_manager.speech_pause()
				emit_signal("finished")
				queue_free()
			else:
				text[cur_page] = text[cur_page].replace("\n"," ")
				page_chars = text[cur_page].length()
				cur_char = 0
				
				$Text.set_visible_characters(0)
				$Text.set_bbcode(text[cur_page])
				$Timer.start()
			
		else:
			$Text.set_visible_characters(page_chars)
			$Timer.stop()
			cur_char = page_chars


func _on_Timer_timeout():
	if cur_char < page_chars:
		cur_char += 1
		$Text.set_visible_characters(cur_char)
	else:
		$Timer.stop()
