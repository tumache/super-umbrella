extends Node2D

signal stage_ready(stage)

onready var entities = $Entities

func _ready():
	emit_signal("stage_ready", self)

func _player_died(player):
	$HUD/StageTransition.transition($HUD/StageTransition.DEATH)